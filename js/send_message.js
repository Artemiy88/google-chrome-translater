const $chrome = chrome.storage.sync;
$(function () {
    $(".close").unbind("click").bind("click", function (a) {
        a.preventDefault();
        window.close()
    });
    const b = ["translate_1_butt", "translate_2_butt", "orfo_1_butt", "orfo_2_butt", "translit_1_butt", "translit_2_butt"];
    $.each(b, function (a, d) {
        $chrome.get(d, function (e) {
            const c = e[d];
            if (c) {
                $("." + d).text(c)
            } else {
                $("." + d + ", ." + d + "_plus").addClass("hidden")
            }
        })
    })
});
$().on('keydown', function (d) {
    const e = window.getSelection();
    if (e.type === "Range") {
        const f = ["translate", "orfo", "translit"];
        $.each(f, function (c, b) {
            const h = b + "_1";
            const a = b + "_2";
            switch (b) {
                case "translate":
                    $chrome.get(h, function (g) {
                        $chrome.get(a, function (w) {
                            if (w[a] && w[a] !== "") {
                                let m = false;
                                $().on('keyup', function (i) {
                                    if (i.which === g[h]) {
                                        m = false
                                    }
                                }).on('keydown', function (j) {
                                    if (j.which === g[h]) {
                                        m = true
                                    }
                                    if (window.getSelection) {
                                        const k = window.getSelection();
                                        if (k.rangeCount) {
                                            const i = k.getRangeAt(0).cloneRange();
                                            if (j.which === w[a] && m === true) {
                                                if (isCyrillic(i)) {
                                                    translate(i, "ru-en")
                                                } else {
                                                    translate(i, "en-ru")
                                                }
                                            }

                                        }
                                    }
                                })
                            } else {
                                if (d.which === g[h]) {
                                    if (window.getSelection) {
                                        const n = window.getSelection();
                                        if (n.rangeCount) {
                                            const p = n.getRangeAt(0).cloneRange();
                                            if (isCyrillic(p)) {
                                                translate(p, "ru-en")
                                            } else {
                                                translate(p, "en-ru")
                                            }
                                        }
                                    }
                                }
                            }
                        })
                    });
                    break;
                case "orfo":
                    $chrome.get(h, function (g) {
                        $chrome.get(a, function (x) {
                            if (x[a] && x[a] !== "") {
                                let m = false;
                                $().on('keyup', function (i) {
                                    if (i.which === g[h]) {
                                        m = false
                                    }
                                }).on('keydown', function (j) {
                                    if (j.which === g[h]) {
                                        m = true
                                    }
                                    if (window.getSelection) {
                                        const k = window.getSelection();
                                        if (k.rangeCount) {
                                            const i = k.getRangeAt(0).cloneRange();
                                            if (j.which === x[a] && m === true) {
                                                handleRequest(i);
                                            }
                                        }
                                    }
                                })
                            } else {
                                if (d.which === g[h]) {
                                    if (window.getSelection) {
                                        const n = window.getSelection();
                                        if (n.rangeCount) {
                                            const p = n.getRangeAt(0).cloneRange();
                                            handleRequest(p)
                                        }
                                    }
                                }
                            }
                        })
                    });
                    break;
                case "translit":
                    $chrome.get(h, function (g) {
                        $chrome.get(a, function (q) {
                            if (q[a] && q[a] !== "") {
                                let o = false;
                                $().on('keyup', function (i) {
                                    if (i.which === g[h]) {
                                        o = false
                                    }
                                }).on('keydown', function (j) {
                                    if (j.which === g[h]) {
                                        o = true
                                    }
                                    if (window.getSelection) {
                                        const k = window.getSelection();
                                        if (k.rangeCount) {
                                            const l = k.getRangeAt(0).cloneRange();
                                            const i = auto_layout_keyboard(l.toString());
                                            const newNode = document.createTextNode(i);
                                            if (j.which === q[a] && o === true) {
                                                l.extractContents();
                                                l.insertNode(newNode)
                                            }
                                        }
                                    }
                                })
                            } else {
                                if (d.which === g[h]) {
                                    if (window.getSelection) {
                                        const p = window.getSelection();
                                        if (p.rangeCount) {
                                            const r = p.getRangeAt(0).cloneRange();
                                            const n = auto_layout_keyboard(r.toString());
                                            const newNode = document.createTextNode(n);
                                            r.extractContents();
                                            r.insertNode(newNode)
                                        }
                                    }
                                }
                            }
                        })
                    });
                    break;
                default:
                    return false;
            }
        })
    }
});
let isCyrillic = function (b) {
    return /[а-я]/i.test(b)
};

function translate(e, f) {
    const h = "https://translate.yandex.net/api/v1.5/tr.json/translate";
    $.getJSON(h, {
        key: "trnsl.1.1.20160926T082023Z.296db8ca575a86ba.908c99aa57dc04739854686a124a576c1ab8c74d",
        text: e.toString().replace(/_/g, " "),
        lang: f
    }, function (a) {
        const new_range = a.text.toString();
        const newNode = document.createTextNode(new_range);
        e.extractContents();
        e.insertNode(newNode)
    });
}

function auto_layout_keyboard(b) {
    const replacer = {
        q: "й",
        w: "ц",
        e: "у",
        r: "к",
        t: "е",
        y: "н",
        u: "г",
        i: "ш",
        o: "щ",
        p: "з",
        "[": "х",
        "]": "ъ",
        "{": "Х",
        "}": "Ъ",
        a: "ф",
        s: "ы",
        d: "в",
        f: "а",
        g: "п",
        h: "р",
        j: "о",
        k: "л",
        l: "д",
        ";": "ж",
        "'": "э",
        ":": "Ж",
        '"': "Э",
        z: "я",
        x: "ч",
        c: "с",
        v: "м",
        b: "и",
        n: "т",
        m: "ь",
        ",": "б",
        ".": "ю",
        "`": "ё",
        "~": "Ё",
        "<": "Б",
        ">": "Ю",
        "/": ".",
        "?": ",",
        "^": ":",
        "@": '"',
        "&": "?"
    };
    return b.replace(/[A-z/,.~&?@:;><'"\]\[{}]/g, function (a) {
        return a === a.toLowerCase() ? replacer[a] : replacer[a.toLowerCase()].toUpperCase()
    })
}

function handleRequest(b) {
    getCheckResult(b.toString(), function (a) {
        const e = JSON.parse(a);
        const f = e.matches[0].replacements[0].value;
        if (e.matches.length <= 0) {
            return false
        }
        const new_range = f.toString();
        const newNode = document.createTextNode(new_range);
        b.extractContents();
        b.insertNode(newNode)
    })
}

function getCheckResult(m, k) {
    const j = 'https://languagetool.org/api/v2/';
    const o = new XMLHttpRequest();
    o.timeout = 60 * 1000;
    const p = j + (j.endsWith("/") ? "check" : "/check");
    o.open("POST", p);
    o.onload = function () {
        const a = o.response;
        k(a);
    };
    const n = "webextension-chrome";
    const l = "disabledRules=WHITESPACE_RULE&useragent=" + n + "&text=" + encodeURIComponent(m) + "&language=ru-RU";
    o.send(l)
}

function dd (text) {
    console.log(text);
}
